

package Control;

import Dao.PersonaDAO;
import Model.Persona;
import java.util.ArrayList;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

public class PersonaDaoImpl implements PersonaDAO{

    private Session sesion = null;
    private SessionFactory sesionFactory = HibernateUtil.getSessionFactory();
    
    @Override
    public void save(Persona p) {
        throw new IllegalArgumentException("Metodo no implementado");
    }

    @Override
    public void modify(Persona p) {
        throw new IllegalArgumentException("Metodo no implementado");
    }

    @Override
    public void delete(Persona p) {
        throw new IllegalArgumentException("Metodo no implementado");
    }

    @Override
    public Persona find(int id) {
        Persona p = null;
            try {
            sesion = sesionFactory.openSession();
            sesion.beginTransaction();            
            p = (Persona) sesion.get(Persona.class, id);    
        } catch (HibernateException he) {
            System.out.println(he.getMessage());
        }finally{
            if(sesion!=null)
                sesion.close();
        }
        return p;
    }
    
    @Override
    public Persona login(int user, String pass){        
            Persona p =(Persona) find(user);
            if(p!=null){
                if(p.getClave().equalsIgnoreCase(pass)){
                    return p;
                }
            }
        return null;
    }

    @Override
    public void unactivate(int id) {
         try {            
            sesion = sesionFactory.openSession();
            sesion.beginTransaction();        
                String hql = "UPDATE Persona set estado = inactivo where identificacion = :ident";
                Query query = sesion.createQuery(hql);                
                //query.setString("newstate", "inactivo");
                query.setParameter("ident", id);
                query.executeUpdate();
            sesion.getTransaction().commit();
        } catch (HibernateException e) {
        }
    }

    
   
    
}
