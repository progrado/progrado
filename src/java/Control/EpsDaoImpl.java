package Control;

import Dao.EpsDAO;
import Model.Eps;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;


public class EpsDaoImpl implements EpsDAO{          
    private Session sesion = null;
    private SessionFactory sesionFactory = HibernateUtil.getSessionFactory();
    
    @Override
    public void save(Eps eps) {                   
        try {
            sesion = sesionFactory.openSession();
            sesion.beginTransaction();
            sesion.save(eps);
            sesion.getTransaction().commit();
        } catch (HibernateException he) {
            sesion.getTransaction().rollback();
            System.out.println("Error al Guardar "+he.getMessage());
        }finally{
            if(sesion!=null)
                sesion.close();
        }
        
        
    }

    @Override
    public void modify(Eps eps) {
        try {            
            sesion = sesionFactory.openSession();
            sesion.beginTransaction();
            sesion.merge(eps);
            sesion.getTransaction().commit();           
        } catch (HibernateException he) {
            sesion.getTransaction().rollback();
            System.out.println("Error al Modificar"+he.getMessage());
        }finally{
            if(sesion!=null){
                sesion.close();
            }
        }
        
    }
    
    @Override
    public void delete(Eps eps) {
         try {
            sesion = sesionFactory.openSession();
            sesion.beginTransaction();
            sesion.delete(eps);
            sesion.getTransaction().commit();           
        } catch (HibernateException he) {
            sesion.getTransaction().rollback();
            System.out.println("Error al Eliminar"+he.getMessage());
        }finally{
            if(sesion!=null){
                sesion.close();
            }
        }
    }

    @Override
    public Eps find(int id) {
        Eps eps = null;
        try {
            sesion = sesionFactory.openSession();
            sesion.beginTransaction();            
            eps = (Eps) sesion.get(Eps.class, id);    
        } catch (HibernateException he) {
            System.out.println(he.getMessage());
        }finally{
            if(sesion!=null)
                sesion.close();
        }
        
        return eps;
    }

    @Override
    public Eps findByName(String name){
        Eps eps = null;
        try {            
            sesion = sesionFactory.openSession();
            sesion.beginTransaction();
            String hql = "FROM Eps  where nombre = :n";            
            Query query = sesion.createQuery(hql);     
            query.setParameter("n", name);
            List <Eps> results = query.list();  
            eps = results.get(0);            
        } catch (HibernateException e) {
            System.out.println(e.getMessage());
            
        }finally{
            if(sesion!=null)
                sesion.close();
        }
        return eps;
    }
    
    public ArrayList<Eps> getAll(){
        ArrayList<Eps> eps = new ArrayList<>();
            try {
                sesion = sesionFactory.openSession();
                String hql = "From Eps";
                eps = (ArrayList<Eps>) sesion.createQuery(hql).list();
            } catch (HibernateException e) {
            }finally{
                sesion.close();
            }
        return eps;
    }
    
    public ArrayList<String> getNombres(){
        ArrayList<String> nombres= new ArrayList<>();
        for(Eps eps : getAll()){
            nombres.add(eps.getNombre());
        }
        return nombres;
    }
       
}
