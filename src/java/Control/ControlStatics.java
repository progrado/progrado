/*
 * Progrado..
 */

package Control;

import Model.Persona;
import Model.Statics;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;


public class ControlStatics {

    public int getInfo(Statics enumeration){
        int result = 0;
        try {
            SessionFactory sesionFactory = HibernateUtil.getSessionFactory();
            Session sesion = sesionFactory.openSession();
            sesion.beginTransaction();
            Query query = sesion.createQuery(enumeration.getHql());     
            List<Persona> personas = (List<Persona>) query.list();
            result = personas.size();
        } catch (HibernateException e) {
        }
    
        return result;
    }
}
