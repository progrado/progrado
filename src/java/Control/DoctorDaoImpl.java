package Control;

import Dao.DoctorDAO;
import Model.Doctor;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import javax.faces.application.FacesMessage;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

public class DoctorDaoImpl implements DoctorDAO {

    private Session sesion = null;
    private SessionFactory sesionFactory = HibernateUtil.getSessionFactory();

    @Override
    public void save(Doctor doctor) {        
        try {
            sesion = sesionFactory.openSession();
            sesion.beginTransaction();
            sesion.save(doctor);
            sesion.getTransaction().commit();
            Util.addMessage("Doctor: "+doctor.getNombres()+" Guardado con Exito", FacesMessage.SEVERITY_INFO);
        } catch (HibernateException he) {
            sesion.getTransaction().rollback();
            Util.addMessage("Error, No se ha Podido Insertar ", FacesMessage.SEVERITY_ERROR);
        } finally {
            if (sesion != null) {
                sesion.close();
            }
        }
    }

    @Override
    public void modify(Doctor doctor) {        
        try {
            sesion = sesionFactory.openSession();
            sesion.beginTransaction();
            sesion.merge(doctor);
            sesion.getTransaction().commit();
            Util.addMessage("Doctor Modificado Correctamente", FacesMessage.SEVERITY_INFO);
        } catch (HibernateException he) {
            sesion.getTransaction().rollback();
            System.out.println("Error al Modificar" + he.getMessage());
        } finally {
            if (sesion != null) {
                sesion.close();
            }
        }
    }

    @Override
    public void delete(Doctor doctor) {
        try {                     
            sesion = sesionFactory.openSession();
            sesion.beginTransaction();
            sesion.delete(doctor);
            sesion.getTransaction().commit();
        } catch (HibernateException he) {
            sesion.getTransaction().rollback();
            System.out.println("Error al Modificar" + he.getMessage());
        } finally {
            if (sesion != null) {
                sesion.close();
            }
        }
    }

    @Override
    public Doctor find(int id) {
        Doctor doc = null;
        try {
            sesion = sesionFactory.openSession();
            sesion.beginTransaction();
            doc = (Doctor) sesion.get(Doctor.class, id);
        } catch (HibernateException he) {
            System.out.println(he.getMessage());
        } finally {
//            if (sesion != null) {
//                sesion.close();
//            }
        }

        return doc;
    }

    public ArrayList<Doctor> getAll() {
        ArrayList doctores = null;
        try {
            sesion = sesionFactory.openSession();
            sesion.beginTransaction();
            String hql = "FROM Doctor";
            doctores = (ArrayList<Doctor>) sesion.createQuery(hql).list();
        } catch (HibernateException e) {
            System.out.println(e.getMessage());
        } finally {
            sesion.close();
        }
        return doctores;
    }

    @Override      
    public Doctor findByName(String fullname){
        Doctor doctor = new Doctor();
            try {            
                sesion = sesionFactory.openSession();
                sesion.beginTransaction();
                StringTokenizer st = new StringTokenizer(fullname);
                String nombre = "";
                String ape = "";
                while(st.hasMoreElements()){
                    nombre = st.nextElement().toString().toUpperCase();
                    ape = st.nextElement().toString().toUpperCase();
                }
                String hql = "FROM Doctor where nombres = :nomb AND apellidos = :apell";                
                Query query = sesion.createQuery(hql);                
                query.setParameter("nomb", nombre);
                query.setParameter("apell", ape);
                doctor = (Doctor) query.uniqueResult();
            } catch (HibernateException e) {
            }
        return doctor;
    }

}
