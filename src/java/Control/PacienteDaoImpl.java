package Control;

import Dao.PacienteDAO;
import Model.Paciente;
import javax.faces.application.FacesMessage;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

public class PacienteDaoImpl implements PacienteDAO {

    private SessionFactory sesionFactory = HibernateUtil.getSessionFactory();
    private Session sesion = null;

    @Override
    public void save(Paciente paciente) {       
        try {
            sesion = sesionFactory.openSession();
            sesion.beginTransaction();
            sesion.save(paciente);
            sesion.getTransaction().commit();
            Util.addMessage("Paciente: " + paciente.getNombres() + " Guardado con Exito",
                    FacesMessage.SEVERITY_INFO);
        } catch (HibernateException he) {
            sesion.getTransaction().rollback();
            Util.addMessage("Error al Guardar " + he.getMessage(), FacesMessage.SEVERITY_ERROR);
        } finally {
            if (sesion != null) {
                sesion.close();
            }
        }
    }

    @Override
    public void modify(Paciente paciente) {
        throw new IllegalArgumentException("Metodo no implementado");
    }

    @Override
    public void delete(Paciente paciente) {
        throw new IllegalArgumentException("Metodo no implementado");
    }

    @Override
    public Paciente find(int identificacion) {
        Paciente p = null;
        try {
            sesion = sesionFactory.openSession();
            p = (Paciente) sesion.get(Paciente.class, identificacion);
        } catch (HibernateException e) {
            
        }
        return p;
    }


}
