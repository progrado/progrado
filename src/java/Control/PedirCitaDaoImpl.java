
package Control;

import Dao.DoctorDAO;
import Dao.PedirCitaDAO;
import Model.Doctor;
import Model.Pedircita;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import javax.faces.application.FacesMessage;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

public class PedirCitaDaoImpl implements PedirCitaDAO{

    private Session sesion = null;
    private SessionFactory sesionFactory = HibernateUtil.getSessionFactory();
    
    @Override
    public void save(Pedircita solicitud) {               
        try {            
            if(isValid(solicitud)){
                sesion = sesionFactory.openSession();
                sesion.beginTransaction();
                sesion.save(solicitud);
                sesion.getTransaction().commit();
            }else{
                Util.addMessage("Cita en conflicto", FacesMessage.SEVERITY_ERROR);
            }
        } catch (HibernateException e) {
            Util.addMessage("Error", FacesMessage.SEVERITY_INFO);
            e.getStackTrace();
        }finally{
            sesion.close();
        }
    }

    @Override
    public void modify(Pedircita  solicitud) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete(Pedircita  solicitud) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Pedircita find(int id) {
        Pedircita pedirCita = null;
        try {
            sesion = sesionFactory.openSession();
            pedirCita = (Pedircita) sesion.get(Pedircita.class, id);
        } catch (HibernateException e) {
        }            
        return pedirCita;
    }   

    @Override
    public ArrayList<Pedircita> getAllByDoctor(Doctor d){
        ArrayList<Pedircita> citas = new ArrayList<>();
        DoctorDAO doctorDao = new DoctorDaoImpl();
            try {
                sesion = sesionFactory.openSession();
                sesion.beginTransaction();
                Doctor doc = (Doctor)doctorDao.find(d.getIdentificacion());
                Iterator it = doc.getPedircitas().iterator();
                while(it.hasNext()){
                    citas.add((Pedircita)it.next());
                }
            } catch (HibernateException e) {
        
            }finally{
                sesion.close();
            }
        return citas;
    }        
    
     @Override
    public ArrayList<Pedircita> getByDay(Timestamp d){
        ArrayList<Pedircita> solicitudes = null;
        try {
            Timestamp fechafin = new Timestamp(d.getTime());
            Timestamp start = new Timestamp(d.getTime());
            start.setHours(00);
            start.setMinutes(00);
            start.setSeconds(00);
            start.setNanos(00);            
            fechafin.setHours(19);
            fechafin.setMinutes(30);
            fechafin.setSeconds(00);
            fechafin.setNanos(0);            
            sesion = sesionFactory.openSession();
            sesion.beginTransaction();
            String hql = "FROM Pedircita where fecha between :start and :end";
            Query query = sesion.createQuery(hql);
            query.setParameter("start", start);        
            query.setParameter("end", fechafin);
            solicitudes = (ArrayList<Pedircita>)query.list();            
        } catch (HibernateException e) {
            e.getStackTrace();
        } finally{
            sesion.close();
        }       
        return solicitudes;
    }        
    
    boolean isValid(Pedircita pc){   
        boolean valido = true;
        ArrayList<Pedircita> solicitudes = getByDay(pc.getFecha());                
            for (Pedircita pedircita : solicitudes) {                
                Timestamp timestamp = new Timestamp(pedircita.getFecha().getTime());                    
                /**
                 * Validacion para Comprobar si 2 fechas son iguales para un mismo doctor en una
                 * misma solicitud
                 */
                if( (pc.getFecha().compareTo(timestamp) == 0 || isBefore(pc.getFecha())) &&
                        pc.getDoctores().getIdentificacion() == pedircita.getDoctores().getIdentificacion()){
                    valido = false;                    
                    //break;
                }
            }
        return valido;
    }
  

    boolean isBefore(Timestamp d){
        return d.compareTo(new Timestamp(new Date().getTime())) <= 0;
    }
         
   
}
