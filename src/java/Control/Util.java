package Control;

import java.util.StringTokenizer;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

public class Util {
  
    public static void addMessage(String msj, FacesMessage.Severity severidad) {
        FacesMessage mensaje = new FacesMessage(severidad, msj, null);
        FacesContext.getCurrentInstance().addMessage(null, mensaje);
    }

  }
