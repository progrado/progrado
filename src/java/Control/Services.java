
package Control;

import java.util.ArrayList;
import org.hibernate.HibernateException;


public interface Services {    
    public void Save(Object o);
    public void Modify(Object o);
    public void Delete(Object o);
    public Object Find(int id);
    public void Unactivate(int id);
}
