
package Model;


public enum Statics {
    
    NUMBEROFPEOPLE("FROM Persona"),
    NUMBEROFMAN("FROM Persona where genero = 'Masculino'"),
    NUMBEROFWOMAN("FROM Persona where genero = 'femenino'"),
    TOTALDATES("FROM Cita"),
    TOTALSOLICITUDES("FROM Pedircita");
    
    
    private String hql;
    
    Statics(String sentence){
        hql = sentence;
    }
    
    public String getHql(){
        return hql;
    }
}
