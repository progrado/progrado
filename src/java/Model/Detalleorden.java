package Model;
// Generated 13/06/2014 09:38:01 PM by Hibernate Tools 3.6.0



/**
 * Detalleorden generated by hbm2java
 */
public class Detalleorden  implements java.io.Serializable {


     private int iddetalleorden;
     private OrdenMedica ordenMedica;
     private Medicamento medicamentos;

    public Detalleorden() {
    }

	
    public Detalleorden(int iddetalleorden) {
        this.iddetalleorden = iddetalleorden;
    }
    public Detalleorden(int iddetalleorden, OrdenMedica ordenMedica, Medicamento medicamentos) {
       this.iddetalleorden = iddetalleorden;
       this.ordenMedica = ordenMedica;
       this.medicamentos = medicamentos;
    }
   
    public int getIddetalleorden() {
        return this.iddetalleorden;
    }
    
    public void setIddetalleorden(int iddetalleorden) {
        this.iddetalleorden = iddetalleorden;
    }
    public OrdenMedica getOrdenMedica() {
        return this.ordenMedica;
    }
    
    public void setOrdenMedica(OrdenMedica ordenMedica) {
        this.ordenMedica = ordenMedica;
    }
    public Medicamento getMedicamentos() {
        return this.medicamentos;
    }
    
    public void setMedicamentos(Medicamento medicamentos) {
        this.medicamentos = medicamentos;
    }




}


