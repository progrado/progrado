/*
 * Progrado..
 */

package Dao;

import Model.Paciente;

/**
 *
 * @author dmartinez
 */
public interface PacienteDAO {
    public void save(Paciente paciente);
    public void modify(Paciente paciente);
    public Paciente find(int identificacion);
    public void delete(Paciente paciente);
            
}
