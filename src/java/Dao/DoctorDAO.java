/*
 * Progrado..
 */

package Dao;

import Model.Doctor;
import java.util.ArrayList;

/**
 *
 * @author dmartinez
 */
public interface DoctorDAO {
    public void save(Doctor doctor);
    public void modify(Doctor doctor);
    public void delete(Doctor doctor);
    public Doctor find(int identificacion);
    public Doctor findByName(String fullname);
    public ArrayList<Doctor> getAll();
}
