/*
 * Progrado..
 */

package Dao;

import Model.Doctor;
import Model.Pedircita;
import java.sql.Timestamp;
import java.util.ArrayList;

public interface PedirCitaDAO {
    public void save(Pedircita solicitud);
    public void modify(Pedircita solicitud);
    public void delete(Pedircita solicitud);
    public Pedircita find(int id);
    public ArrayList<Pedircita> getAllByDoctor(Doctor doctor);
    public ArrayList<Pedircita> getByDay(Timestamp d);
}
