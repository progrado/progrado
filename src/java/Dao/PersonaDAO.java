/*
 * Progrado..
 */

package Dao;

import Model.Persona;

/**
 *
 * @author dmartinez
 */
public interface PersonaDAO {
    public void save(Persona p);
    public Persona login(int user, String pass);
    public void delete(Persona p);
    public void modify(Persona p);
    public void unactivate(int id);
    public Persona find(int id);
}
