/*
 * Progrado..
 */

package Dao;

import Model.Eps;
import java.util.ArrayList;

/**
 *
 * @author dmartinez
 */
public interface EpsDAO {
    public void save(Eps eps);
    public void modify(Eps eps);
    public void delete(Eps eps);
    public Eps find(int id);
    public Eps findByName(String name);
    public ArrayList<Eps> getAll();
    public ArrayList<String> getNombres();
}
