/*
 * Progrado..
 */
package Beans;

import Control.PacienteDaoImpl;
import Control.DoctorDaoImpl;
import Control.EpsDaoImpl;
import Control.PedirCitaDaoImpl;
import Control.Util;
import Dao.PedirCitaDAO;
import Model.Doctor;
import Model.Paciente;
import Model.Pedircita;
import java.io.Serializable;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.event.ScheduleEntryMoveEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultScheduleEvent;
import org.primefaces.model.DefaultScheduleModel;
import org.primefaces.model.ScheduleEvent;
import org.primefaces.model.ScheduleModel;

@ManagedBean
@ViewScoped
public class BeanSchedule implements Serializable {

    private ScheduleModel eventModel;
    private ScheduleEvent event = new DefaultScheduleEvent();    
    private Date selected;
    private Timestamp time;    
    private Pedircita pedircita;
    private Doctor doctor;
    private List<String> eps;
    
    
    public BeanSchedule() {
        eventModel = new DefaultScheduleModel();        
        eps = new EpsDaoImpl().getNombres();
        //LoadEvents();
    }

    void LoadEvents() {
        eventModel.clear();
        PedirCitaDaoImpl controlpc = new PedirCitaDaoImpl();
        DoctorDaoImpl cd = new DoctorDaoImpl();
        //Doctor doc = (Doctor) cd.Find(doctor.getIdentificacion());
        for (Pedircita p : controlpc.getAllByDoctor(doctor)) {
            Timestamp t = new Timestamp(p.getFecha().getTime());
            t.setHours(p.getFecha().getHours() + 1);
            t.setMinutes(p.getFecha().getMinutes());
            t.setSeconds(00);
            t.setNanos(0);
            Date d2 = new Date();
            d2.setTime(t.getTime());
            event = new DefaultScheduleEvent(new PacienteDaoImpl().find(p.getPacientes().getIdentificacion()).toString(), p.getFecha(), d2);
            eventModel.addEvent(event);
        }

    }

    public ScheduleModel getEventModel() {
        return eventModel;
    }

    public ScheduleEvent getEvent() {
        return event;
    }

    public void setEvent(ScheduleEvent event) {
        this.event = event;
    }
   

    public Pedircita getPedircita() {
        return pedircita;
    }

    public void setPedircita(Pedircita pedircita) {
        this.pedircita = pedircita;
    }

    public void addEvent() {        
        PedirCitaDAO pcDao = new PedirCitaDaoImpl();
        Doctor d = (Doctor) new DoctorDaoImpl().find(125);
        Paciente paciente = (Paciente) new PacienteDaoImpl().find(123);
        Pedircita p = new Pedircita();
        p.setFecha(time);
        p.setDoctores(d);
        p.setPacientes(paciente);
        pcDao.save(p);
        LoadEvents();
    }

    public void onEventSelect(SelectEvent selectEvent) {
        event = (ScheduleEvent) selectEvent.getObject();
    }

    public void onDateSelect(SelectEvent selectEvent) {
        selected = (Date) selectEvent.getObject();
        //selected.setHours(selected.getHours()-5);     
        String f = String.valueOf(selected.getTime());
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyy HH:mm");
        time = new Timestamp(Long.parseLong(f));
        time.setHours(selected.getHours());
        Util.addMessage(format.format(time), FacesMessage.SEVERITY_INFO);
    }

    public void onEventMove(ScheduleEntryMoveEvent event) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Event moved", 
                "Day delta:" + event.getDayDelta() + ", Minute delta:" + event.getMinuteDelta());

        addMessage(message);
    }

    public void onRowSelect(org.primefaces.event.SelectEvent se) {
        eventModel.clear();
        doctor = (Doctor) se.getObject();
        LoadEvents();
        Util.addMessage("Se han actualizado las citas del/la doctor/a: "
                +doctor.getNombres(), FacesMessage.SEVERITY_INFO);
    }

    private void addMessage(FacesMessage message) {
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

    public Date getSelected() {
        return selected;
    }

    public void setSelected(Date hora) {
        this.selected = hora;
    }   

    public Doctor getDoctor() {
        return doctor;
    }

    public void setDoctor(Doctor doctor) {
        this.doctor = doctor;
    }

    public List<String> getEps() {
        return eps;
    }

    
}
