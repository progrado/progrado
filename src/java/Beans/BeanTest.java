/*
 * Progrado..
 */
package Beans;

import Control.Util;
import Model.Doctor;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;

@ManagedBean

public class BeanTest {

    private Timestamp time;
    private hola h;
    private Doctor selected;    
    
    public BeanTest() {
    }

    public void onDateSelect(SelectEvent event) {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        time = new Timestamp( ((Date) event.getObject()).getTime() );
    }

    public Timestamp getTime() {
        return time;
    }

    public void setTime(Timestamp time) {
        this.time = time;
    }

    public hola getH() {
        return h;
    }

    public void setH(hola h) {
        this.h = h;
    }
    
    
    public Doctor getSelected() {
        return selected;
    }

    public void setSelected(Doctor selected) {
        this.selected = selected;
    }

   
   public void onRowSelect(org.primefaces.event.SelectEvent se){
       selected = (Doctor) se.getObject();
       Util.addMessage(selected.getNombres(), FacesMessage.SEVERITY_INFO);
   }
    
    
    class hola{
        String jeje;

        public String getJeje() {
            return jeje;
        }
        
        
    }
    

}
