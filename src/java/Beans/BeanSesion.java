
package Beans;

import Control.PersonaDaoImpl;
import Dao.PersonaDAO;
import Model.Persona;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;


@ManagedBean
@SessionScoped
public class BeanSesion {        
    private FacesMessage facesMessage;
    private Persona p;
    int usr;
    String contra;
    PersonaDAO personaDao = new PersonaDaoImpl();

    public BeanSesion() {                
//        HttpSession session = (HttpSession)context.getExternalContext().getSession(false);                      
    }
    
    public String Iniciar(){
        String rute=null;
           p = personaDao.login(usr,contra);
            if(p==null){
                facesMessage = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Datos Incorrectos", null);
                FacesContext.getCurrentInstance().addMessage(null,facesMessage);                
            }else{
                facesMessage = new FacesMessage(FacesMessage.SEVERITY_INFO, this.toString(), null);
                FacesContext.getCurrentInstance().addMessage(null,facesMessage);                
                switch(p.getTipo()){
                    case "recepcionista":
                            rute = "recep";
                        break;       
                    case "admin":
                        rute = "admin";
                        break;
                    case "doctor":
                        rute = "doctor";
                        break;                                                        
                }                
            }
                    
        return rute;
    }
    
    public String Logut(){
        HttpServletRequest http = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        http.getSession().invalidate();
        return "home";
        
    }

    public Persona getP() {
        return p;
    }

    public void setP(Persona p) {
        this.p = p;
    }

    public int getUsr() {
        return usr;
    }

    public void setUsr(int usr) {
        this.usr = usr;
    }

    public String getContra() {
        return contra;
    }

    public void setContra(String contra) {
        this.contra = contra;
    }
                
    @Override
    public String toString(){
        String user = p.getNombres() + " " + p.getApellidos();
        String wel = "Usted ha iniciado sesion como "+ user;        
        return wel;
    }

    
}
