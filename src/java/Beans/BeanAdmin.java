

package Beans;

import Control.DoctorDaoImpl;
import Control.Util;
import Model.Doctor;
import java.util.ArrayList;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.event.RowEditEvent;
import Dao.DoctorDAO;

@ManagedBean

public class BeanAdmin {

    private Doctor doctor;    
    private DoctorDAO doctroDao = new DoctorDaoImpl();    
    private ArrayList<Doctor> doctores;
    
    public BeanAdmin() {
        doctor = new Doctor();
        doctores = doctroDao.getAll();                     
    }

    //Getters & Setters                
    public Doctor getDoctor() {
        return doctor;
    }

    public void setDoctor(Doctor doctor) {
        this.doctor = doctor;
    }

    public ArrayList<Doctor> getDoctores() {
        return doctores;
    }

    public void setDoctores(ArrayList<Doctor> doctores) {
        this.doctores = doctores;
    }
    //----------------------------------------------------
 
    //Metodos Controladores
    public void Guardar(){
        doctor.setEstado("habilitado");
        doctor.setClave("12345");      
        doctor.setTipo("doctor");        
        doctroDao.save(doctor);        
    }
    
    public void Modificar(RowEditEvent event){
        doctroDao.modify((Doctor)event.getObject());
    }
    
    // -----------------------
}
